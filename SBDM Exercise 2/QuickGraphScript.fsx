﻿#r @"C:\Users\Anders Bech Mellson\Desktop\FSharp\SBDM Exercise 2\packages\QuickGraph.3.6.61119.7\lib\net4\QuickGraph.dll"

open QuickGraph
open QuickGraph.Algorithms.Observers
open QuickGraph.Algorithms
open QuickGraph.Algorithms.ShortestPath
open System

let graph = new AdjacencyGraph<string, Edge<string>>()
let rec buildGraph (names: string list) =
    match names with
      | head :: tail ->
        for name in tail do
          graph.AddVerticesAndEdge(new Edge<string>(head,name)) |> ignore
          graph.AddVerticesAndEdge(new Edge<string>(name,head)) |> ignore
        buildGraph tail
      | [] -> ()

let getDistance source target =
    let distanceFunc = Func<Edge<string>, float>(fun e -> 1.0)
    let dijkstra = new DijkstraShortestPathAlgorithm<string, Edge<string>>(graph, distanceFunc)
    let vis = new VertexPredecessorRecorderObserver<string,Edge<string>>()
    using (vis.Attach(dijkstra)) (fun x ->
      dijkstra.Compute(source)
      let (success,collection) = vis.TryGetPath(target)
      match success with
        | true ->
          for c in collection do
            printfn "%O" c
          Some((collection |> List.ofSeq).Length)
        | false -> None )

let names1 = ["Anders";"Ditte";"Ellen"]
let names2 = ["Kaspar";"Sara";"Aksel"]
let names3 = ["Anders";"Peter";"Hanne"]
buildGraph names1
buildGraph names2
buildGraph names3

getDistance "Ditte" "Hanne"

graph
for v in graph.Vertices do
  printfn "%O" v