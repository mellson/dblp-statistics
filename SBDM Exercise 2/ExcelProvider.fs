﻿module ExcelProvider

// Open the Excel file with the F# type provider
open FSharp.ExcelProvider
type ITU_Faculty_Members = ExcelFile<"ITU_Faculty_Members.xlsx">
let facultyMembersFile = new ITU_Faculty_Members()

// Get all entities from the file which are not null and convert it into a list
let noNullNames = facultyMembersFile.Data |> Seq.filter (fun facultyMember -> not (facultyMember.Name = null)) |> List.ofSeq

// The ITU data puts lastname first before a comma.
// This function restores a firstname before lastname order.
let cleanName (name:string) =
  let indexOfComma = name.IndexOf(",")
  let firstName = name.Substring(indexOfComma + 2)
  let lastName = name.Substring(0, indexOfComma)
  (firstName + " " + lastName)

// Extract all the names and put them in a list
let names():string list = noNullNames |> List.map (fun facultyMember -> cleanName facultyMember.Name) |> Seq.ofList |> Seq.distinct |> List.ofSeq