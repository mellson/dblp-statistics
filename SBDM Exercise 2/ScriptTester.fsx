﻿#r "C:\Users\Anders Bech Mellson\Desktop\FSharp\SBDM Exercise 2\packages\FSharp.Data.2.0.15\lib\portable-net40+sl5+wp8+win8\FSharp.Data.dll"
#r "System.Xml.Linq.dll"

open FSharp.Data

type DBLP = XmlProvider<"data\dblp_sample.xml">
let sample = DBLP.Parse("data\dblp_sample.xml")

printfn "%d" sample.Articles.Length

type Author = XmlProvider<"""<author name="Paul Feyerabend" born="1924" />""">
let sample2 = Author.Parse("""<author name="Karl Popper" born="1902" />""")

printfn "%s (%d)" sample2.Name sample2.Born