﻿module DblpLoader

open System.Xml
open System.IO

// A dblp entity consists of a title combined with a list of author names
type dblpEntity = string * string list

// Load the dblp entities from the dblp.xml file
let loadDblpEntities limit =
  let mutable dblpEntities:dblpEntity list = List.empty
  let settings = new XmlReaderSettings(DtdProcessing = DtdProcessing.Parse, ValidationType = ValidationType.DTD)
  use reader = XmlReader.Create("dblp.xml", settings)
  let mutable keepReading = 0
  while reader.Read() && keepReading < limit do
    if reader.NodeType = XmlNodeType.Element then
      match reader.Name with
        | "article"
        | "inproceedings"
        | "proceedings"
        | "book"
        | "incollection"
        | "phdthesis"
        | "mastersthesis" ->
          let dplbRecordType = reader.Name
          let mutable authors:string list = List.empty
          let mutable title = ""
          while not (reader.NodeType = XmlNodeType.EndElement && reader.Name = dplbRecordType) do
            try
              if reader.Read() && not (reader.NodeType = XmlNodeType.EndElement) then
                match reader.Name with
                  | "author" -> authors <- reader.ReadElementContentAsString() :: authors
                  | "title" -> title <- reader.ReadElementContentAsString()
                  | _        -> ()
            with
              | ex -> ()
          keepReading <- keepReading + 1
          dblpEntities <- (title,authors) :: dblpEntities
        | _ -> ()
  dblpEntities