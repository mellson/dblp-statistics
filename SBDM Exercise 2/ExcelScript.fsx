﻿#r @"C:\Users\Anders Bech Mellson\Desktop\FSharp\SBDM Exercise 2\packages\ExcelProvider.0.1.2\lib\net40\ExcelProvider.dll"

// Open the Excel file with the F# type provider
open FSharp.ExcelProvider
type ITU_Faculty_Members = ExcelFile<"ITU_Faculty_Members.xlsx">
let facultyMembersFile = new ITU_Faculty_Members()

// Get all entities from the file which are not null and convert it into a list
let noNullNames = facultyMembersFile.Data |> Seq.filter (fun facultyMember -> not (facultyMember.Name = null)) |> List.ofSeq

// Extract all the names and put them in a list
let names:string list = noNullNames |> List.map (fun facultyMember -> facultyMember.Name)

// The ITU data puts lastname first before a comma.
// This function restores a firstname before lastname order.
// It returns the name split into a string list.
let splitName (name:string) =
  let indexOfComma = name.IndexOf(",")
  let firstName = name.Substring(indexOfComma + 2)
  let lastName = name.Substring(0, indexOfComma)
  (firstName + " " + lastName)

// Convert all names to string lists with splitName
let splitNames = names |> List.map (fun name -> splitName name)

for facultyMemberNames in splitNames do
  printfn "%O" facultyMemberNames