﻿module Program

open DblpLoader
open ExcelProvider
open DblpAnalysis
open StringHelper

let start (sw:System.Diagnostics.Stopwatch) = sw.Start()
let stop (sw:System.Diagnostics.Stopwatch) =
  System.Console.WriteLine("Time elapsed: {0}\n", sw.Elapsed)
  sw.Stop()
  sw.Reset()

[<EntryPoint>]
let main argv =
  let sw = System.Diagnostics.Stopwatch()

  start sw
  // Get all ITU current and former faculty members
  let ituFacultyMembers = names()
  printfn "%d %s" ituFacultyMembers.Length " faculty members was loaded"
  stop sw

  start sw
  let numberOfEntitiesToLoad = System.Int32.MaxValue
  let dblpEntities = loadDblpEntities numberOfEntitiesToLoad
  printfn "%d %s" dblpEntities.Length " dblp enties was loaded"
  stop sw
  
  start sw
  let titles = findPublications ituFacultyMembers dblpEntities
  let mutable titleMap = Map.empty
  for (title,author) in titles do
    if not (titleMap.ContainsKey author) then
      titleMap <- titleMap.Add(author,[title])
    else
      titleMap <- titleMap.Add(author,title :: Map.find author titleMap)

  printfn "%d %s" titles.Length ("entities was found for ITU faculty members")
  printfn "%d %s" titleMap.Count ("authors from ITU has records in the dblp file")
  stop sw

  start sw
  printfn "Building graph"
  buildGraphFromDblp dblpEntities
  stop sw

  let printDistanceInfo name1 name2 =
      let optInt = (getGraphDistance name1 name2)
      if optInt.IsNone then printfn "There is no connection between %s and %s" name1 name2
      else printfn "The distance between %s and %s is %i" name1 name2 optInt.Value

  start sw
  let name1 = "Philippe Bonnet"
  let name2 = "Peter Sestoft"
  let name3 = "Jim Gray"
  let name4 = "Pierre Dupont"
  printDistanceInfo name1 name2
  printDistanceInfo name1 name3
  printDistanceInfo name1 name4
  stop sw

  let k = System.Console.ReadKey()
  0 // return an integer exit code
