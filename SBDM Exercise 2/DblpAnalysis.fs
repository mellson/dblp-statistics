﻿module DblpAnalysis

open StringHelper
open DblpLoader

// Checks that the Levenshtein distance between the two names are equal or below 1
let (|WithinLevenshteinLimit|_|) (name1:string) (name2:string) =
  if levenshtein name1 name2 <= 1
    then Some() else None

// Checks if the name is contained in the authors list using the Levenshtein distance
let rec contains author (authors: string list) =
    match authors with
      | WithinLevenshteinLimit author :: _ -> true
      | nameNotInAuthors :: tail           -> contains author tail
      | []                                 -> false

// Gets all the titles a given author has written
let rec findPublications (authorList:string list) (list:dblpEntity list) =
    match list with
      | head :: tail ->
        match head with
          | (title, authors) ->
              let mutable titles: (string*string) list = []
              for author in authorList do
               if authors |> List.exists (fun n -> n = author) then titles <- (title,author) :: titles
              if not titles.IsEmpty then titles @ findPublications authorList tail
              else findPublications authorList tail
      | [] -> []

let rec findPublications2 author (list:dblpEntity list) =
    match list with
      | head :: tail ->
        match head with
          | (title, authors) ->
              let mutable titles: dblpEntity list = []
              if authors |> List.exists (fun n -> n = author) then titles <- head :: titles
              if not titles.IsEmpty then titles @ findPublications2 author tail
              else findPublications2 author tail
      | [] -> []

let mutable (dblp:dblpEntity list) = []
let mutable found = false
let rec distance author person coauthersSearched depth =
    match found with
      | true -> [None]
      | false ->
        let titles = findPublications2 author dblp
        let coauthors = titles |> List.collect (fun (title, authors) -> authors) |> Seq.ofList |> Seq.distinct |> List.ofSeq
        let filteredCoauthors = (Set.ofList coauthors) - (Set.ofList coauthersSearched) |> List.ofSeq
        if filteredCoauthors |> List.exists (fun name -> name = person) then
          found <- true
          [Some(depth)]
        elif filteredCoauthors.IsEmpty then [None]
        else filteredCoauthors |> List.collect (fun coauthor -> distance coauthor person ((filteredCoauthors @ coauthersSearched) |> Seq.ofList |> Seq.distinct |> List.ofSeq ) (depth + 1))

let getDistance author person dblp2 =
    if dblp.IsEmpty then dblp <- dblp2
    let distances = distance author person [author] 0
    let values = distances |> List.choose id
    if values.IsEmpty then -1
    else values |> List.min

open QuickGraph
open QuickGraph.Algorithms.Observers
open QuickGraph.Algorithms
open QuickGraph.Algorithms.ShortestPath
open System

let graph = new AdjacencyGraph<string, Edge<string>>()
let rec buildGraph (names: string list) =
    match names with
      | head :: tail ->
        for name in tail do
          graph.AddVerticesAndEdge(new Edge<string>(head,name)) |> ignore
          graph.AddVerticesAndEdge(new Edge<string>(name,head)) |> ignore
        buildGraph tail
      | [] -> ()

let buildGraphFromDblp (dblp:dblpEntity list) =
    for (title, authors) in dblp do
      buildGraph authors

let getGraphDistance source target =
    let distanceFunc = Func<Edge<string>, float>(fun e -> 1.0)
    let dijkstra = new DijkstraShortestPathAlgorithm<string, Edge<string>>(graph, distanceFunc)
    let vis = new VertexPredecessorRecorderObserver<string,Edge<string>>()
    using (vis.Attach(dijkstra)) (fun x ->
      dijkstra.Compute(source)
      let (success,collection) = vis.TryGetPath(target)
      match success with
        | true ->
          for c in collection do
            printfn "%O" c
          Some((collection |> List.ofSeq).Length)
        | false -> None )